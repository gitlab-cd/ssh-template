## ssh helper [![pipeline status](https://gitlab.com/gitlab-cd/ssh-template/badges/master/pipeline.svg)](https://gitlab.com/gitlab-cd/ssh-template/pipelines/)
ssh helper ease remote connection from gilab-ci to your ssh host.  
This library provides two commands, **ssh_init** (optional) and **ssh_run**.  


### setup 
- First, Add the private key (i.e: SSH_PRIVATE_KEY or SSH_TOKEN) as a variable to your gitlab project or group.  
![](https://docs.gitlab.com/ee/ci/variables/img/variables.png)
Note : you should not flag this variable as Protected if you want to use this variable with non-protected branch, otherwise ssh will fail.

- Then include the ssh.yml script from your .gitlab-ci.yml
```yaml
include: 'https://gitlab.com/gitlab-cd/ssh-template/raw/master/ssh.yml'
````

### ssh_run
Simply authenticate and execute remote command on a single line.
> **ssh_run** USER HOSTNAME $SSH_PRIVATE_KEY COMMAND  

```yaml
include: 'https://gitlab.com/gitlab-cd/ssh-template/raw/master/ssh.yml'

clean:
stage: build
script:
- ssh_run "root" "myownserver.com" "$SSH_PRIVATE_KEY" "rm -rf .cache"
```

### ssh_init
setup your SSH connection, then use any vanilla ssh command.  
> **ssh_init** $SSH_PRIVATE_KEY HOSTNAME    

Suppose i've declared my SSH key in the gitlab variable SSH_PRIVATE_KEY.  
Hereunder our use case :  
> As root user,  
> i want to ssh connect to 'myownserver.com',  
> in order to deploy and restart my server app.

Example implementation in .gitlab-ci.yml :  
```yaml
include: 'https://gitlab.com/gitlab-cd/ssh-template/raw/master/ssh.yml'

before_scripts:
- ssh_init "$SSH_PRIVATE_KEY" "myownserver.com"

promote:
stage: deploy
script:
- ssh root@myownserver.com "sudo pkill -9 java;
            wget https://gitlab.com/myownproject/-/jobs/120093218/artifacts/file/myownserverapp.jar;
            java -jar myownserverapp.jar"
- ssh root@myownserver.com "ps aux | grep myownserverapp"
```

###  Integration note
By design in ssh.yml, the ssh helper is initialized during the `before_script` gitlab step.  
Note it won't work anymore if you define your own `berore_script` section in  `.gitlab-ci.yml`.  
If you want to define your own `berore_script`, be sure you add a call to `ssh_helper` in that section :  
```yaml
before_script:
- *ssh_helper
- echo "then, your own instructions..."
```


